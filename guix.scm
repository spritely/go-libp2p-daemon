;; This file is waived into the public domain, as-is, no warranty provided.
;;
;; If the public domain doesn't exist where you live, consider
;; this a license which waives all copyright and neighboring intellectual
;; restrictions laws mechanisms, to the fullest extent possible by law,
;; as-is, no warranty provided.
;;
;; No attribution is required and you are free to copy-paste and munge
;; into your own project.
(use-modules (guix)
             (guix packages)
             (guix git-download)
             (gnu packages ipfs)
             (srfi srfi-1))

(define (keep-file? file stat)
  (not (any (lambda (my-string)
              (string-contains file my-string))
            (list ".git" "guix.scm"))))

(package
 (inherit spritely-libp2p-daemon)
 (version "0.1.0-git")
 (source (local-file (dirname (current-filename))
                     #:recursive? #t
                     #:select? keep-file?)))
