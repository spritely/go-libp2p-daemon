package main

import (
	"bufio"
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p/core/crypto"
	"github.com/libp2p/go-libp2p/core/host"
	"github.com/libp2p/go-libp2p/core/network"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/libp2p/go-libp2p/core/peerstore"
	"github.com/multiformats/go-multiaddr"
)

func findInConfigData(config []string, needle string) []string {
	var found = []string{}
	for _, part := range config {
		parts := strings.Split(part, ":")
		if parts[0] == needle {
			found = append(found, parts[1])
		}
	}
	return found
}

func decodePrivateKey(encoded_private_key string) (crypto.PrivKey, error) {
	var private_key crypto.PrivKey
	var err error
	if encoded_private_key == "" {
		private_key, _, err = crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, rand.Reader)
	} else {
		decoded_private_key, err := base64.StdEncoding.DecodeString(encoded_private_key)
		if err != nil {
			fmt.Println("Got error decoding base64 key:", err)
			return nil, err
		}
		private_key, err = crypto.UnmarshalPrivateKey(decoded_private_key)
		fmt.Println("Unmarshalled key:", private_key, err)
	}
	return private_key, err
}

func configDataToConfig(config []string) ([]libp2p.Option, crypto.PrivKey, error) {
	// Read private key is provided.
	encoded_private_key := findInConfigData(config, "private-key")
	var private_key crypto.PrivKey
	var err error
	if len(encoded_private_key) <= 0 {
		fmt.Println("Generating new key")
		private_key, _, err = crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, rand.Reader)
	} else {
		fmt.Println("Provided with key:", encoded_private_key)
		private_key, err = decodePrivateKey(encoded_private_key[0])
	}
	if err != nil {
		return nil, nil, err
	}
	// Read addresses if provided.
	provided_addresses := findInConfigData(config, "address")
	if len(provided_addresses) <= 0 {
		provided_addresses = append(provided_addresses, "/ip4/0.0.0.0/tcp/0")
	}

	opts := []libp2p.Option{
		libp2p.Identity(private_key),
		libp2p.EnableNATService(),
		libp2p.NATPortMap(),
		libp2p.ListenAddrStrings(provided_addresses...),
		// Enable Kademlia DHT
		// libp2p.Routing(func(h host.Host) (routing.PeerRouting, error) {
		// 	idht, err = dht.New(ctx, h)
		// 	return idht, err
		// }),
	}
	return opts, private_key, nil
}

func newLibp2pFromConfig(ctx context.Context, config []string) (host.Host, crypto.PrivKey, error) {
	opts, privkey, err := configDataToConfig(config)
	if err != nil {
		return nil, nil, err
	}
	host, err := libp2p.New(opts...)
	if err != nil {
		return nil, nil, err
	}

	// By default libp2p only accepts the external IP after 4 connections...
	// that's good but we need at least 3 connections then.
	// these are the IPFS bootstrap nodes, make the connections to them.
	// https://github.com/ipfs/kubo/blob/v0.21.0/config/bootstrap_peers.go#L11C1-L24C2
	bootstrapNodes := []string{
		"/dns4/sv15.bootstrap.libp2p.io/tcp/443/wss/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN",
		"/ip4/104.131.131.82/tcp/4001/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",      // mars.i.ipfs.io
		"/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ", // mars.i.ipfs.io
		"/ip4/139.178.65.157/tcp/4001/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa",
		"/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN",
		"/dnsaddr/bootstrap.libp2p.io/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa",
		"/dnsaddr/bootstrap.libp2p.io/p2p/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb",
		"/dnsaddr/bootstrap.libp2p.io/p2p/QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt",
	}

	// Start by looking at how many addresses we have, we're looking for
	// the external addresses to be added below
	initialAddrNum := len(host.Addrs())

	for _, node := range bootstrapNodes {
		info, err := peer.AddrInfoFromString(node)
		if err != nil {
			log.Println(err)
			continue
		}
		err = host.Connect(ctx, *info)
		if err != nil {
			log.Println(err)
			continue
		}
		log.Println("Connected to bootstrap node:", node)
	}

	msSpent := 0
	for msSpent < 5000 {
		// When we have more addresses than when we started.
		if len(host.Addrs()) > initialAddrNum {
			break
		}
		time.Sleep(100 * time.Millisecond)
		msSpent = msSpent + 100
	}

	return host, privkey, nil
}

func listenForIncomingConnections(ctx context.Context, host host.Host, config []string) {
	incoming_path := findInConfigData(config, "incoming")
	protocol := findInConfigData(config, "protocol")
	version := findInConfigData(config, "version")
	if len(incoming_path) <= 0 || len(protocol) <= 0 || len(version) <= 0 {
		log.Println(
			"Missing config info",
			"incoming_path:", incoming_path,
			"protocol:", protocol,
			"version:", version,
		)
		return
	}

	// TODO: use the given: protocol + version
	host.SetStreamHandler("/ocapn/1.0.0", func(s network.Stream) {
		sock, err := net.Dial("unix", incoming_path[0])
		if err != nil {
			log.Println("Could not handle new connection", err, incoming_path[0])
			return
		}
		log.Println("New inbound connection!")
		go relayIncoming(ctx, s, sock)
		go relayOutgoing(ctx, s, sock)
	})
}

func listenForOutgoingConnections(ctx context.Context, host host.Host, config []string) {
	outgoing_path := findInConfigData(config, "outgoing")
	protocol := findInConfigData(config, "protocol")
	version := findInConfigData(config, "version")
	if len(outgoing_path) <= 0 || len(protocol) <= 0 || len(version) <= 0 {
		log.Println(
			"Missing config info",
			"outgoing_path:", outgoing_path,
			"protocol:", protocol,
			"version:", version,
		)
		return
	}

	l, err := net.Listen("unix", outgoing_path[0])
	if err != nil {
		log.Println("Could not open unix socket at:",
			outgoing_path, "error:", err)
		return
	}
	for {
		sock, err := l.Accept()
		if err != nil {
			log.Fatal("Could not accept new connection:", err)
			return
		}
		go newOutgoingConnection(ctx, host, sock, protocol[0], version[0])
	}
}

func addAddrsToPeerstore(host host.Host, addrs []string) (peer.ID, error) {
	var id peer.ID
	peer_store := host.Peerstore()
	for _, addr := range addrs {
		// Parse the address from a string to a multiaddr
		maddr, err := multiaddr.NewMultiaddr(addr)
		if err != nil {
			return "", err
		}
		// Extract the peer ID from the multiaddr.
		info, err := peer.AddrInfoFromP2pAddr(maddr)
		if err != nil {
			return "", err
		}

		if id != "" && id != info.ID {
			return "", errors.New("The addresses do not all go to the same peer ID")
		}
		id = info.ID
		peer_store.AddAddrs(info.ID, info.Addrs, peerstore.PermanentAddrTTL)

	}
	return id, nil
}

func newOutgoingConnection(
	ctx context.Context, host host.Host,
	sock net.Conn, protocol string, version string) {

	scanner := bufio.NewScanner(sock)
	var id peer.ID
	var err error
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\n")
		chunks := strings.Split(line, " ")
		if chunks[0] == "CONNECT" {
			id, err = addAddrsToPeerstore(host, chunks[1:])
			if err != nil {
				log.Println("Got error:", err)
			}
			break
		} else {
			log.Println("Recieved unknown command:", string(chunks[0]))
		}
	}

	s, err := host.NewStream(context.Background(), id, "/ocapn/1.0.0")
	if err != nil {
		log.Println("Error creating stream to:", id, "error:", err)
		return
	}
	go relayIncoming(ctx, s, sock)
	go relayOutgoing(ctx, s, sock)
}

func relayIncoming(ctx context.Context, s network.Stream, sock net.Conn) {
	defer sock.Close()
	defer s.Close()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return
		default:
			buf := make([]byte, 1024)
			nr, err := io.ReadAtLeast(s, buf, 32)
			if err != nil {
				log.Println("Error during processing of incoming message", err)
				return
			}
			//log.Println("[relay in] ", string(buf[0:nr]))
			sock.Write(buf[0:nr])
		}
	}
}

func relayOutgoing(ctx context.Context, s network.Stream, sock net.Conn) {
	defer sock.Close()
	defer s.Close()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return
		default:
			buf := make([]byte, 1024)
			nr, err := sock.Read(buf)
			if err != nil {
				return
			}
			//log.Println("[relay out] ", string(buf[0:nr]))
			s.Write(buf[0:nr])
		}
	}
}

func setupNewLibp2pConnection(ctx context.Context, config []string) (host.Host, crypto.PrivKey, error) {
	host, privkey, err := newLibp2pFromConfig(ctx, config)
	if err != nil {
		return nil, nil, err
	}
	go listenForIncomingConnections(ctx, host, config)
	go listenForOutgoingConnections(ctx, host, config)
	return host, privkey, nil
}

func getHostAddresses(host host.Host) []string {
	// Build host multiaddress
	hostAddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/p2p/%s", host.ID()))
	var p2pAddrs []string

	for _, addr := range host.Addrs() {
		addr_str := addr.Encapsulate(hostAddr).String()
		p2pAddrs = append(p2pAddrs, addr_str)
	}
	return p2pAddrs

}

func newControlConnection(ctx context.Context, sock net.Conn) {
	defer sock.Close()
	scanner := bufio.NewScanner(sock)
	var hosts []host.Host

	defer func() {
		for _, host := range hosts {
			host.Close()
		}
	}()

	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\n")
		chunks := strings.Split(line, " ")
		fmt.Println("line:", line)
		if chunks[0] == "NEW" {
			host, privkey, err := setupNewLibp2pConnection(ctx, chunks[1:])
			hosts = append(hosts, host)
			addrs := getHostAddresses(host)
			if err != nil {
				log.Println("Error setting up new Libp2p connection", err)
				continue
			}
			key_data, err := crypto.MarshalPrivateKey(privkey)
			encoded_key := base64.StdEncoding.EncodeToString(key_data)
			if err != nil {
				log.Println("Error marshalling private key", err)
			}

			for _, addr := range addrs {
				sock.Write([]byte(" address:"))
				sock.Write([]byte(addr))
			}

			sock.Write([]byte(" private-key:"))
			sock.Write([]byte(encoded_key))
			sock.Write([]byte("\n"))
		} else {
			log.Println("Error, unknown command:", string(line))
		}
	}
	fmt.Println("End of newControlConnection")
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	control_socket_path := flag.String("listen", "/tmp/libp2p-control.sock", "Unix socket path for the control socket where the daemon will listen")
	flag.Parse()

	l, err := net.Listen("unix", *control_socket_path)
	if err != nil {
		log.Fatal("Could not listen on control socket:", err)
		return
	}

	cleanup := func() {
		l.Close()
		os.Remove(*control_socket_path)
	}
	defer cleanup()

	// Deferred functions don't wrong if the OS tells the program to end abructly
	// catch that and run the cleanup
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sigChan
		cleanup()
		os.Exit(0)
	}()

	log.Println("Daemon running and listening on", *control_socket_path)

	for {
		sock, err := l.Accept()
		if err != nil {
			log.Fatal("Could not accept new connection to control socket:", err)
			return
		}

		go newControlConnection(ctx, sock)
	}
}
