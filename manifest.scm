(use-modules (guix)
             (guix packages)
             (guix build-system trivial))

(define spritely-libp2p-daemon
  (load (string-append (dirname (current-filename)) "/guix.scm")))

(define spritely-libp2p-daemon-binary
  (package
    (name "spritely-libp2p-daemon")
    (version "0.1")
    (source #f)
    (build-system trivial-build-system)
    (native-inputs
     (list spritely-libp2p-daemon))
    (synopsis #f)
    (description #f)
    (home-page #f)
    (license #f)
    (arguments
     (list #:modules '((guix build utils))
           #:builder
           #~(begin
              (use-modules (guix build utils))
              (let ((bin (string-append #$output "/bin"))
                    (libp2p-daemon (assoc-ref %build-inputs "spritely-libp2p-daemon")))
                (mkdir-p bin)
                (copy-file (string-append libp2p-daemon "/bin/spritely-libp2p-daemon")
                           (string-append bin "/spritely-libp2p-daemon"))))))))


(packages->manifest (list spritely-libp2p-daemon-binary))
